'use strict';

(function () {

  window.utils = window.utils || {};

  var utils = window.utils;

  /** Underscore templating settings */

  _.templateSettings = {
    evaluate: /<@(.+?)@>/g,
    interpolate: /<@-(.+?)@>/g,
    escape: /<@=(.+?)@>/g
  };

  utils.i18n = {};
  utils.i18n['validation.required'] = '必填';
  utils.i18n['validation.invalid_input'] = '输入内容无效';
  utils.i18n['validation.range_max_begin'] = '数据数量必须大于等于';
  utils.i18n['validation.range_min_end'] = '并且小于等于';
  utils.i18n['validation.range_max'] = '数据数量必须大于等于';
  utils.i18n['validation.range_min'] = '数据数量必须小于等于';
  utils.i18n['label.confirm_delete'] = '确定删除？';
  utils.i18n['label.error'] = '错误';
  utils.i18n['label.wait'] = '等待';

  /** Popup view */

  var PopupView = Backbone.View.extend({

    events: {
      'click #popup_ok, #popup_confirm': 'ok',
      'click #popup_cancel': 'cancel'
    },

    initialize: function () {
      // initialize modal
      this.$el.modal({
        show: false
      });
      this.$el.on('show.bs.modal', function () {
        $('body').addClass('popup-modal-backdrop-container');
      });
      this.$el.on('hidden.bs.modal', function () {
        $('body').removeClass('popup-modal-backdrop-container');
      });
      // reset positive flag
      this.$el.on('shown.bs.modal', function () {
        this._positive = false;
      }.bind(this));
    },

    alert: function (message) {
      this.$el.find('.popup-modal-content').removeClass('has-input');
      this.$el.find('.popup-message').text(message);
      this.$el.find('.popup-input').addClass('hidden');
      this.$el.find('#popup_alertActions').removeClass('hidden');
      this.$el.find('#popup_confirmActions').addClass('hidden');
      this.$el.modal('show');

      var resolver = null;
      this.$el.one('hidden.bs.modal', function () {
        resolver(this._positive);
      }.bind(this));

      return new Promise(function (resolve) {
        resolver = resolve;
      });
    },

    confirm: function (message) {
      this.$el.find('.popup-modal-content').removeClass('has-input');
      this.$el.find('.popup-message').text(message);
      this.$el.find('.popup-input').addClass('hidden');
      this.$el.find('#popup_alertActions').addClass('hidden');
      this.$el.find('#popup_confirmActions').removeClass('hidden');
      this.$el.modal('show');

      var resolver = null;
      this.$el.one('hidden.bs.modal', function () {
        resolver(this._positive);
      }.bind(this));

      return new Promise(function (resolve) {
        resolver = resolve;
      });
    },

    prompt: function (message, inputPlaceholder) {
      this.$el.find('.popup-modal-content').addClass('has-input');
      this.$el.find('.popup-message').text(message);
      this.$el.find('.popup-input').removeClass('hidden');
      this.$el.find('.popup-input textarea').prop('placeholder', inputPlaceholder);
      this.$el.find('#popup_alertActions').addClass('hidden');
      this.$el.find('#popup_confirmActions').removeClass('hidden');
      this.$el.modal('show');

      var resolver = null;
      this.$el.one('hidden.bs.modal', function () {
        resolver({positive: this._positive, input: this.$el.find('.popup-input textarea').val().trim()});
      }.bind(this));

      return new Promise(function (resolve) {
        resolver = resolve;
      });
    },

    ok: function () {
      this._positive = true;
      this.$el.modal('hide');
    },

    cancel: function () {
      this.$el.modal('hide');
    }

  });

  utils.popup = new PopupView({el: '#popup_modal'});

  /** Toast view */

  var ToastView = Backbone.View.extend({

    defaults: {
      _hideAlertTimeoutId: null
    },

    info: function (message, autoHide) {
      this._show(message, _.isUndefined(autoHide) ? true : autoHide, 'alert-info');
    },

    warning: function (message, autoHide) {
      this._show(message, _.isUndefined(autoHide) ? true : autoHide, 'alert-warning');
    },

    error: function (message, autoHide) {
      this._show(message, autoHide, 'alert-danger');
    },

    success: function (message, autoHide) {
      this._show(message, _.isUndefined(autoHide) ? true : autoHide, 'alert-success');
    },

    hide: function () {
      this.$el.removeClass('on');
      this.$el.addClass('off');
    },

    _show: function (message, autoHide, styleClass) {
      var alertContent = this.$el.find('.alert-content');

      // auto hide for success alert
      clearTimeout(this._hideAlertTimeoutId);
      if (autoHide) {
        this._hideAlertTimeoutId = setTimeout(this.hide.bind(this), 5000);
      }

      var html = '<div class="alert ' + styleClass + '" role="alert">' +
        '<button type="button" class="close"><span>&times;</span></button>' + message +
        '</div>';
      var alertElement = $(html);
      alertElement.find('button').click(this.hide.bind(this));
      alertContent.html('');
      alertElement.appendTo(alertContent);

      if (this.$el.hasClass('on')) {
        // animate in again
        this.$el.removeClass('animated');
        this.$el.addClass('off');
        this.$el.removeClass('on');
        setTimeout(function () {
          this.$el.addClass('animated');
          this.$el.removeClass('off');
          this.$el.addClass('on');
        }.bind(this), 10);
      } else {
        this.$el.removeClass('off');
        this.$el.addClass('on');
      }
    }
  });

  utils.toast = new ToastView({el: '#alert_dialog'});

  /** Form and inputs */

  function getFormGroup(element) {
    element = $(element);
    var formGroup = element.find('.form-group');
    if (!formGroup.length) {
      formGroup = element.closest('.form-group');
    }
    var helpBlock = formGroup.find('.help-block');
    var input = formGroup.find('input');
    if (!input.length) {
      input = formGroup.find('.form-control');
    }
    // for date picker
    var button = formGroup.find('button');
    var setError = function (error) {
      if (error) {
        this.formGroup.addClass('has-error');
        this.helpBlock.text(error);
      } else {
        this.formGroup.removeClass('has-error');
        this.helpBlock.text('');
      }
    };
    var setMessage = function (message) {
      this.formGroup.removeClass('has-error');
      this.helpBlock.html(message);
    };
    var getValue = function () {
      if (this.input.length > 1) {
        var values = [];
        var multi = false;
        _.each(this.input, function (choice) {
          choice = $(choice);
          if (choice.prop('type') === 'checkbox') {
            // for checkbox, we should return an array of values
            multi = true;
          }
          if (choice.is(':checked')) {
            values.push(choice.val());
          }
        });
        return multi ? values : values[0];
      } else {
        return this.input.val();
      }
    };
    var plainSetValue = function (value) {
      if (this.input.length > 1) {
        var values = _.isArray(value) ? value : [value];
        values = values.map(function (v) {
          if (!_.isString(v)) {
            return v + '';
          }
          return v;
        });
        _.each(this.input, function (choice) {
          choice = $(choice);
          if (_.contains(values, choice.val())) {
            choice.prop('checked', true);
          } else {
            choice.prop('checked', false);
          }
        });
      } else {
        this.input.val(value);
      }
    };
    var setValue = function (value) {
      this.plainSetValue(value);
      this.formGroup.data('input-value', value);
      this.setError(null);
      element.trigger('change');
    };
    var plainResetValue = function () {
      if (this.input.length > 1) {
        _.each(this.input, function (choice) {
          choice = $(choice);
          choice.prop('checked', false);
        });
      } else {
        this.input.val('');
      }
    };
    var resetValue = function () {
      this.plainResetValue();
      this.formGroup.removeData('input-value');
      this.setError(null);
      element.trigger('change');
    };
    var resetValidation = function () {
      this.formGroup.removeData('input-value');
      this.setError(null);
    };

    return {
      formGroup: formGroup,
      input: input,
      button: button,
      helpBlock: helpBlock,
      setError: setError,
      setMessage: setMessage,
      getValue: getValue,
      plainSetValue: plainSetValue,
      setValue: setValue,
      plainResetValue: plainResetValue,
      resetValue: resetValue,
      resetValidation: resetValidation
    };
  }

  function validateFormGroup(formGroup, validate, forced) {
    var value = formGroup.getValue();

    if (formGroup.input.length === 1 && _.isString(value) && value !== value.trim()) {
      // trim all user inputs
      value = value.trim();
      formGroup.setValue(value);
    }

    // skip validation is value is valid and remain unchanged
    var validatedValue = formGroup.formGroup.data('input-value');
    if (!forced && !_.isUndefined(validatedValue) && value === validatedValue) {
      return Promise.resolve({
        valid: true,
        changed: false,
        value: formGroup.formGroup.data('input-value')
      });
    }

    formGroup.setMessage('<i class="fa fa-spinner fa-spin"></i>');

    return Promise
      .try(function () {
        return validate(value);
      })
      .then(function (value) {
        formGroup.setMessage('');
        return {
          valid: true,
          changed: true,
          value: value
        };
      })
      .catch(function (error) {
        formGroup.setError(error || utils.i18n['validation.invalid_input']);
        return {
          valid: false,
          changed: true
        };
      })
      .then(function (result) {
        if (_.isUndefined(result.value)) {
          formGroup.formGroup.removeData('input-value');
        } else {
          formGroup.formGroup.data('input-value', result.value);
        }
        return result;
      });
  }

  function validateForm(form) {
    var valid = true;
    var formValue = {};
    var promises = [];
    _.each(_.keys(form), function (key) {
      var value = form[key];
      if (_.isFunction(value.then)) {
        promises.push(value.then(function (result) {
          valid = valid && result.valid;
          if (result.valid) {
            formValue[key] = result.value;
          }
        }));
      } else if (_.isObject(value)) {
        promises.push(validateForm(value).then(function (result) {
          valid = valid && result.valid;
          if (result.valid) {
            formValue[key] = result.value;
          }
        }));
      } else {
        formValue[key] = value;
      }
    });
    return Promise
      .all(promises)
      .then(function () {
        return {
          valid: valid,
          value: formValue
        };
      });
  }

  utils.getFormGroup = getFormGroup;
  utils.validateFormGroup = validateFormGroup;
  utils.validateForm = validateForm;

  function getValidator(options) {
    return function (value) {
      var isEmpty = _.isUndefined(value) || _.isNull(value) || value.length === 0;
      if (options.required) {
        if (isEmpty) {
          throw utils.i18n['validation.required'];
        }
      }
      if (options.pattern) {
        if (!isEmpty && !new RegExp(options.pattern).test(value)) {
          throw options.patternErrorMessage || utils.i18n['validation.invalid_input'];
        }
      }
      if (options.jsValidator) {
        return eval(options.jsValidator)(value);
      }
      return value;
    };
  }

  //TODO getValue, change, resetValidation for all inputs

  var InputReference = Backbone.View.extend({

    events: {
      'click .title .actions .btn-success': '_add',
      'click .line .actions .btn-primary': '_update',
      'click .line .actions .btn-danger': '_remove'
    },

    initialize: function () {
      // init template
      var template = _.template(this.$el.find('script').html());
      this._createRow = function (value) {
        return $(template({
          value: value
        }));
      };

      this._options = this.$el.data('input-options');

      var tbody = this.$el.find('table tbody');
      _.each(this._options.values, function (value) {
        tbody.append(this._createRow(value));
      }, this);
    },

    validate: function () {
      var title = this.$el.find('.title');
      var message = this.$el.find('.title .message');

      title.removeClass('has-error');
      message.text('');

      return Promise
        .bind(this)
        .then(function () {
          var numberError = null;
          if (!_.isUndefined(this._options.min) && !_.isUndefined(this._options.max)) {
            numberError = utils.i18n['validation.range_max_begin'] + this._options.min + utils.i18n['validation.range_min_end'] + this._options.max;
          } else if (!_.isUndefined(this._options.min)) {
            numberError = utils.i18n['validation.range_max'] + this._options.min;
          } else if (!_.isUndefined(this._options.max)) {
            numberError = utils.i18n['validation.range_min'] + this._options.max;
          }
          var rows = this.$el.find('table tbody tr');
          if ((!_.isUndefined(this._options.min) && rows.length < this._options.min) ||
            (!_.isUndefined(this._options.max) && rows.length > this._options.max)) {
            title.addClass('has-error');
            message.text(numberError);
            return {valid: false};
          }
          var values = [];
          _.each(rows, function (row) {
            row = $(row);
            values.push(row.data('input-value'));
          }, this);
          return {valid: true, value: values};
        });
    },

    setValue: function (values) {
      var tbody = this.$el.find('table tbody');
      tbody.html('');
      _.each(values, function (value) {
        tbody.append(this._createRow(value));
      }, this);
      this._refresh();
    },

    resetValue: function () {
      this.setValue([]);
    },

    _add: function () {
      eval(this._options.jsEditor)()
        .bind(this)
        .then(function (value) {
          if (value) {
            this.$el.find('table tbody').append(this._createRow(value));
            this._refresh();
          }
        });
    },

    _update: function (e) {
      var row = $(e.target).closest('tr');
      var value = row.data('input-value');
      eval(this._options.jsEditor)(value)
        .bind(this)
        .then(function (newValue) {
          if (newValue) {
            var newRow = this._createRow(newValue);
            row.replaceWith(newRow);
          }
        });
    },

    _remove: function (e) {
      var row = $(e.target).closest('tr');
      utils.popup
        .confirm(utils.i18n['label.confirm_delete'])
        .bind(this)
        .then(function (result) {
          if (result) {
            row.remove();
            this._refresh();
          }
        });
    },

    _refresh: function () {
      var empty = this.$el.find('.empty');
      var table = this.$el.find('table');
      var isEmpty = table.find('tbody tr').length === 0;
      empty.toggleClass('hidden', !isEmpty);
      table.toggleClass('hidden', isEmpty);

      this.validate();
    }
  });

  var InputStatic = Backbone.View.extend({

    validate: function () {
      return Promise.resolve({valid: true, value: this.getValue()});
    },

    getValue: function () {
      return this.$el.find('.form-control-static').text().trim();
    },

    setValue: function (value) {
      this.$el.find('.form-control-static').text(value);
    },

    resetValue: function () {
      this.$el.find('.form-control-static').text('');
    }
  });

  var InputHidden = Backbone.View.extend({

    validate: function () {
      return Promise.resolve({valid: true, value: this.$el.val()});
    },

    getValue: function () {
      return this.$el.val();
    },

    setValue: function (value) {
      this.$el.val(value);
    },

    resetValue: function () {
      this.$el.val('');
    }
  });

  var InputGeneral = Backbone.View.extend({

    events: {
      'focus .form-control': '_focus',
      'blur .form-control': '_blur',
      'click .input-group-btn .btn': '_click'
    },

    initialize: function () {
      this._formGroup = getFormGroup(this.$el);
      this._options = this.$el.data('input-options');
    },

    getFormGroup: function () {
      return this._formGroup;
    },

    validate: function (force) {
      return validateFormGroup(this._formGroup, getValidator(this._options), force);
    },

    setValue: function (value) {
      this._formGroup.setValue(value);
    },

    resetValue: function () {
      this._formGroup.resetValue();
    },

    _focus: function () {
      // store the value to track change
      this._formGroup.formGroup.data('input-previous-value', this._formGroup.getValue());
    },

    _blur: function () {
      if (this._formGroup.formGroup.data('input-previous-value') !== this._formGroup.getValue()) {
        this.validate();
      }
    },

    _click: function (e) {
      var button = $(e.target);

      this._formGroup.setError(null);
      utils.setButtonLoading(button);
      return eval(this._options.jsAction)()
        .bind(this)
        .then(function () {
          // success
          if (this._options.actionTimeout) {
            this._cooldown(this._options.actionTimeout);
          } else {
            utils.resetButtonLoading(button);
          }
        })
        .catch(function (e) {
          this._formGroup.setError(e || utils.i18n['label.error']);
          utils.resetButtonLoading(button);
        });
    },

    _cooldown: function (timeout) {
      var button = this.$el.find('.input-group-btn .btn');
      if (timeout > 0) {
        button.text(utils.i18n['label.wait'] + '... (' + timeout + ')');
        setTimeout(function () {
          this._cooldown(timeout - 1);
        }.bind(this), 1000);
      } else {
        utils.resetButtonLoading(button);
      }
    }
  });

  var InputText = InputGeneral;
  var InputTextWithAction = InputGeneral;
  var InputPassword = InputGeneral;
  var InputTextarea = InputGeneral;

  var InputRichText = InputGeneral.extend({
    initialize: function () {
      this._options = this.$el.data('input-options');
      var summernoteOptions = this._options.summernoteOptions || {};

      var input = this.$el.find('.summernote-editor');
      input.summernote(summernoteOptions);
      this._formGroup = getFormGroup(this.$el);
      this._formGroup.input = input;
      this._formGroup.getValue = function () {
        return input.summernote('code');
      };
      this._formGroup.plainSetValue = function (value) {
        input.summernote('code', value || '');
      };
      this._formGroup.plainResetValue = function () {
        input.summernote('code', '');
      };

      if (this._options.readonly) {
        input.summernote('disable');
      }
    }
  });

  var InputSelect = InputGeneral.extend({
    setSelections: function (selections) {
      var selector = this.$el.find('select');
      selector.html('');
      // add empty option
      var option = $('<option></option>');
      selector.append(option);
      selections.forEach(function (select) {
        var option = $('<option></option>');
        option.text(select.label);
        option.attr('value', select.value);
        selector.append(option);
      });
    }
  });

  var InputDate = InputGeneral.extend({
    initialize: function () {
      this._options = this.$el.data('input-options');
      var datePickerOptions = _.clone(this._options.datePickerOptions || {});
      datePickerOptions.enableOnReadonly = false;
      datePickerOptions.autoclose = true;

      var input = this.$el.find('.input-group.date');
      input.datepicker(datePickerOptions);
      this._formGroup = getFormGroup(this.$el);
      this._formGroup.input = input;
      this._formGroup.getValue = function () {
        return input.datepicker('getDate');
      };
      this._formGroup.plainSetValue = function (value) {
        if (value) {
          input.datepicker('setDate', new Date(value));
        } else {
          input.datepicker('setDate', null);
        }
      };
      this._formGroup.plainResetValue = function () {
        input.datepicker('setDate', null);
      };
      if (this._options.value) {
        var theDate = this._options.value;
        input.datepicker('setDate', new Date(theDate));
      }
    }
  });

  var InputChoices = Backbone.View.extend({

    events: {
      'change input': '_change'
    },

    initialize: function () {
      this._formGroup = getFormGroup(this.$el);
      this._options = this.$el.data('input-options');
    },

    getFormGroup: function () {
      return this._formGroup;
    },

    validate: function (force) {
      return validateFormGroup(this._formGroup, getValidator(this._options), force);
    },

    setValue: function (value) {
      this._formGroup.setValue(value);
    },

    resetValue: function () {
      this._formGroup.resetValue();
    },

    _change: function () {
      this.validate();
    }
  });

  var InputCheckbox = InputChoices;
  var InputRadio = InputChoices;

  var InputFile = Backbone.View.extend({

    events: {
      'change .upload-item-add input': '_upload',
      'click .upload-item .btn-delete': '_delete'
    },

    initialize: function () {
      this._options = this.$el.data('input-options');

      // init template
      var template = _.template(this.$el.find('script').html());
      this._createItemElement = function (value) {
        return $(template({
          value: value,
          getUrl: this._getUrlParser(),
          getName: this._getNameParser(),
          readonly: this._options.readonly
        }));
      };

      // rewrite form group methods
      this._formGroup = getFormGroup(this.$el);
      var inputContainer = this.$el.find('.input-container');
      var uploadItemAddElement = inputContainer.find('.upload-item-add');
      this._formGroup.getValue = function () {
        var values = _.map(inputContainer.find('.upload-item'), function (formControl) {
          formControl = $(formControl);
          return formControl.data('input-value');
        });
        if (this._options.multi) {
          return values;
        } else {
          return values[0] || null;
        }
      }.bind(this);
      this._formGroup.plainSetValue = function (value) {
        uploadItemAddElement.prevAll('.upload-item').remove();
        var values = this._options.multi ? value : [value];
        _.each(values, function (value) {
          if (!value) {
            return;
          }
          this._createItemElement(value).insertBefore(uploadItemAddElement);
        }, this);
        this._refresh(false);
      }.bind(this);
      this._formGroup.plainResetValue = function () {
        uploadItemAddElement.prevAll('.upload-item').remove();
        this._refresh(false);
      }.bind(this);

      // set url parser
      if (this._options.jsUrlParser) {
        this._getUrlParser = function () {
          return eval(this._options.jsUrlParser);
        };
      } else {
        this._getUrlParser = function () {
          return function (value) {
            return value;
          };
        };
      }

      // set name parser
      if (this._options.jsNameParser) {
        this._getNameParser = function () {
          return eval(this._options.jsNameParser);
        };
      } else {
        this._getNameParser = function () {
          return function (value) {
            if (value) {
              var index = value.lastIndexOf('/');
              if (index >= 0) {
                return value.substring(index + 1);
              }
            }
            return value;
          };
        };
      }

      // set uploader
      if (this._options.uploadPath) {
        var that = this;
        this._getUploader = function () {
          return function (file) {
            var data = new FormData();
            data.append('file', file);

            var param = {
              method: 'POST',
              url: that._options.uploadPath,
              contentType: false,
              processData: false,
              dataType: 'json',
              data: data
            };
            return Promise
              .resolve($.ajax(param));
          };
        };
      } else {
        this._getUploader = function () {
          return eval(this._options.jsUploader);
        };
      }

      // create elements
      // NOTE: delay this so in case callback is defined after form initialization
      setTimeout(function () {
        if (this._options.value) {
          var values = _.isArray(this._options.value) ? this._options.value : [this._options.value];
          _.each(values, function (value) {
            this._createItemElement(value).insertBefore(uploadItemAddElement);
          }, this);
          this._refresh(false);
        }
      }.bind(this), 10);
    },

    getFormGroup: function () {
      return this._formGroup;
    },

    validate: function (force) {
      return validateFormGroup(this._formGroup, getValidator(this._options), force);
    },

    setValue: function (value) {
      this._formGroup.setValue(value);
    },

    resetValue: function () {
      this._formGroup.resetValue();
    },

    _upload: function (e) {
      var file = e.target.files[0];
      if (!file) {
        return;
      }

      var inputContainer = this.$el.find('.input-container');
      var uploadItemAddElement = inputContainer.find('.upload-item-add');
      var uploadLabel = uploadItemAddElement.find('label');

      if (this._uploading) {
        return;
      }

      this._uploading = true;

      var fileInput = $(e.target);
      fileInput.prop('disabled', true);
      var forInput = uploadLabel.attr('for');
      uploadLabel.removeAttr('for');
      utils.setButtonLoading(uploadLabel, true);

      this._getUploader()(file)
        .bind(this)
        .then(function (data) {
          this._createItemElement(data).insertBefore(uploadItemAddElement);
          this._refresh(true);
        })
        .catch(function (error) {
          console.error(error);
          // do nothing
        })
        .finally(function () {
          this._uploading = false;
          fileInput.prop('disabled', false);
          fileInput.val(null);
          uploadLabel.attr('for', forInput);
          utils.resetButtonLoading(uploadLabel);
        });
    },

    _delete: function (e) {
      var imageElement = $(e.target).closest('.upload-item');

      utils.popup
        .confirm(utils.i18n['label.confirm_delete'])
        .bind(this)
        .then(function (result) {
          if (result) {
            imageElement.remove();
            this._refresh(true);
          }
        });
    },

    _refresh: function (validate) {
      var inputContainer = this.$el.find('.input-container');
      if (!this._options.multi) {
        var uploadItemAddElement = inputContainer.find('.upload-item-add');
        uploadItemAddElement.toggleClass('hidden', inputContainer.find('.upload-item').length > 0);
      }

      if (validate) {
        this.validate();
      }
    }
  });

  var InputImage = InputFile;

  utils.FormView = Backbone.View.extend({

    initialize: function () {
      this._inputByName = {};
      _.each(this.$el.find('.input-field'), function (inputField) {
        inputField = $(inputField);
        var options = inputField.data('input-options');
        var name = options.name;
        if (this._inputByName[name]) {
          throw 'Duplicated input name: "' + name + '"';
        }
        switch (inputField.data('input-type')) {
          case 'static':
            this._inputByName[name] = new InputStatic({el: inputField});
            break;
          case 'hidden':
            this._inputByName[name] = new InputHidden({el: inputField});
            break;
          case 'text':
            this._inputByName[name] = new InputText({el: inputField});
            break;
          case 'date':
            this._inputByName[name] = new InputDate({el: inputField});
            break;
          case 'textWithAction':
            this._inputByName[name] = new InputTextWithAction({el: inputField});
            break;
          case 'password':
            this._inputByName[name] = new InputPassword({el: inputField});
            break;
          case 'textarea':
            this._inputByName[name] = new InputTextarea({el: inputField});
            break;
          case 'richText':
            this._inputByName[name] = new InputRichText({el: inputField});
            break;
          case 'select':
            this._inputByName[name] = new InputSelect({el: inputField});
            break;
          case 'checkbox':
            this._inputByName[name] = new InputCheckbox({el: inputField});
            break;
          case 'radio':
            this._inputByName[name] = new InputRadio({el: inputField});
            break;
          case 'file':
            this._inputByName[name] = new InputFile({el: inputField});
            break;
          case 'image':
            this._inputByName[name] = new InputImage({el: inputField});
            break;
        }
      }, this);
      _.each(this.$el.find('.input-reference'), function (reference) {
        reference = $(reference);
        var options = reference.data('input-options');
        var name = options.name;
        if (this._inputByName[name]) {
          throw 'Duplicated input name: "' + name + '"';
        }
        this._inputByName[name] = new InputReference({el: reference});
      }, this);
    },

    getInputNames: function () {
      return _.keys(this._inputByName);
    },

    getInput: function (name) {
      return this._inputByName[name];
    },

    validate: function (force) {
      var form = _.reduce(_.keys(this._inputByName), function (form, name) {
        form[name] = this._inputByName[name].validate(force);
        return form;
      }, {}, this);
      return validateForm(form);
    },

    setValue: function (value) {
      _.each(this.getInputNames(), function (name) {
        var input = this.getInput(name);
        var inputValue = value[name];
        if (_.isUndefined(inputValue)) {
          input.resetValue();
        } else {
          input.setValue(value[name]);
        }
      }, this);
    },

    resetValue: function () {
      _.each(this.getInputNames(), function (name) {
        var input = this.getInput(name);
        input.resetValue();
      }, this);
    }

  });

  /** Pagination */

  $(function () {
    var paginationContainer = $('.pagination-container');
    if (paginationContainer.length) {
      pagination(paginationContainer);
    }
  });

  function getQueryForPagination() {
    var query = utils.getQuery();
    delete query.totalPage;
    delete query.totalCount;
    delete query.summary;
    return query;
  }

  function pagination(container) {
    container.find('a.page-num-btn').on('click', function () {
      var query = getQueryForPagination();
      query.pageNumber = parseInt($(this).text());
      window.location.search = $.param(query);
    });

    container.find('.page-arrow-btn').on('click', function () {
      var parent = $(this).closest('li');
      if (parent.hasClass('disabled')) {
        return;
      }
      var page = $(this).data('val');
      var query = getQueryForPagination();
      query.pageNumber = page;
      window.location.search = $.param(query);
    });
  }

  /** Auto-complete Filter */

  $(function () {
    var els = $('.auto-complete');
    _.each(els, function (el) {
      if (!$(el).data('lazy-init')) {
        new utils.AutoComplete({el: el});
      }
    });
  });

  utils.AutoComplete = Backbone.View.extend({

    events: {
      'input .form-control': 'handleInput',
      'focus .form-control': 'handleFocus',
      'blur .form-control': 'handleBlur',
      'click .auto-complete-action .clear-button': 'handleClear',
      'click .auto-complete-popup .auto-complete-popup-item': 'handleItemClick'
    },

    initialize: function () {
      this._options = this.$el.data('options');
      if (_.isUndefined(this._options.value)) {
        this._options.value = null;
      }
      // prevent keypress event to be called too often
      this._loadSelections = _.throttle(this._loadSelections.bind(this), 500, {leading: false});
      this.setValue(this._options.value);
    },

    handleInput: function() {
      if (this._options.freeForm) {
        this._internalSetValue(null, {notify: true, preserveText: true});
      }
      this._loadSelections();
    },

    handleFocus: function () {
      this._focused = true;
      window.clearTimeout(this._timeoutId);
    },

    handleBlur: function () {
      this._focused = false;
      this._timeoutId = window.setTimeout(function () {
        this.$el.find('.auto-complete-popup').addClass('hidden');
        this.$el.find('.auto-complete-action').removeClass('show-loading');
        this._nextLoadingId();
        if (!this._options.freeForm) {
          this._internalSetValue(this.getValue(), {force: true});
        }
      }.bind(this), 500);
    },

    handleClear: function () {
      this._internalSetValue(null, {notify: true});
    },

    handleItemClick: function (e) {
      var itemEl = $(e.target);
      if (!itemEl.hasClass('auto-complete-popup-item')) {
        itemEl = itemEl.closest('.auto-complete-popup-item');
      }
      var value = itemEl.data('value');
      this._internalSetValue(value, {notify: true});
      this.$el.find('.auto-complete-popup').addClass('hidden');
    },

    setValue: function(value) {
      this._internalSetValue(value);
    },

    getValue: function() {
      return this._value;
    },

    getInput: function() {
      return this.$el.find('.form-control');
    },

    _loadSelections: function () {
      var loadingId = this._nextLoadingId();
      var actionEl = this.$el.find('.auto-complete-action');
      actionEl.addClass('show-loading');
      var filterText = this.$el.find('.form-control').val().trim();
      eval(this._options.jsFilter)(filterText)
        .bind(this)
        .then(function (values) {
          if (this._loadingId != loadingId || !this._focused) {
            return;
          }
          var popup = this.$el.find('.auto-complete-popup');
          popup.toggleClass('hidden', _.isEmpty(values));
          popup.html('');
          _.each(values, function (value) {
            var itemEl = $('<div class="auto-complete-popup-item truncate-text">' + this._getItemFormatter()(value, filterText) + '</div>');
            itemEl.data('value', value);
            itemEl.appendTo(popup);
          }.bind(this));
        })
        .finally(function () {
          if (this._loadingId != loadingId || !this._focused) {
            return;
          }
          actionEl.removeClass('show-loading');
        });
    },

    _internalSetValue: function (value, options) {
      options = options || {};
      if (!options.force && this._value === value) {
        return;
      }
      this._value = value;
      if (!options.preserveText) {
        var text = this._getTextFormatter()(this._value);
        this.$el.find('.form-control').val(text);
        this.$el.find('.auto-complete-action').toggleClass('show-clear', !!text && !this._options.freeForm);
      }
      if (options.notify) {
        this.trigger('change', value, this.$el);
        this._getChangeHandler()(value, this.$el);
      }
    },

    _nextLoadingId: function () {
      var loadingId = (this._loadingId || 0) + 1;
      this._loadingId = loadingId;
      return loadingId;
    },

    _getChangeHandler: function() {
      if (this._options.jsChangeHandler) {
        return eval(this._options.jsChangeHandler);
      }
      return function() {};
    },

    _getTextFormatter: function () {
      return this._options.jsTextFormatter ? eval(this._options.jsTextFormatter) : function (value) {
        return value ? value.toString().trim() : '';
      };
    },

    _getItemTextFormatter: function () {
      return this._options.jsItemTextFormatter ? eval(this._options.jsItemTextFormatter) : this._getTextFormatter();
    },

    _getItemFormatter: function () {
      if (this._options.jsItemFormatter) {
        return eval(this._options.jsItemFormatter);
      }
      return function (value, filterText) {
        var text = this._getItemTextFormatter()(value);
        var index = text.indexOf(filterText);
        if (index >= 0) {
          return text.substring(0, index) + '<b>' + filterText + '</b>' + text.substring(index + filterText.length);
        } else {
          return text;
        }
      }.bind(this);
    }
  });

  /** Other utilities */

  var query = (function (params) {
    if (!params || _.isEmpty(params)) {
      return {};
    }
    var query = {};
    for (var i = 0; i < params.length; ++i) {
      if (!params[i]) {
        continue;
      }
      var pair = params[i].split('=', 2);
      var name = pair[0];
      var value = pair.length == 1 ? '' : decodeURIComponent(pair[1].replace(/\+/g, ' '));
      var oldValue = query[name];
      if (_.isUndefined(oldValue)) {
        query[name] = value;
      } else {
        if (!_.isArray(oldValue)) {
          oldValue = [oldValue];
        }
        oldValue.push(value);
      }
    }
    return query;
  })(window.location.search.substr(1).split('&'));

  utils.getQuery = function() {
    return JSON.parse(JSON.stringify(query));
  };

  utils.setQuery = function(newQuery) {
    query = JSON.parse(JSON.stringify(newQuery));
  };

  utils.getQueryWithoutPageNumber = function () {
    return _.omit(utils.getQuery(), 'pageNumber');
  };

  utils.initSelectFilter = function (el, key) {
    $(el).change(function (e) {
      var query = utils.getQueryWithoutPageNumber();
      query[key] = $(e.target).val();
      if (!query[key]) {
        delete query[key];
      }
      window.location.search = $.param(query);
    });
  };

  utils.initButtonFilter = function (el, key) {
    $(el).click(function (e) {
      var query = utils.getQueryWithoutPageNumber();
      query[key] = $(e.target).data('value');
      if (_.isUndefined(query[key])) {
        delete query[key];
      }
      window.location.search = $.param(query);
    });
  };

  utils.initDateFilter = function (el, key, disableClear) {
    el = $(el);
    el.datepicker({clearBtn: !disableClear, language: 'zh-CN', format: 'yyyy-mm-dd'});
    el.change(function () {
      var date = el.val();
      date = date ? utils.getDate(date) : null;
      var query = utils.getQueryWithoutPageNumber();
      if (date) {
        query[key] = date.getTime();
      } else {
        delete query[key];
      }
      window.location.search = $.param(query);
    });
  };

  utils.initNumberFilter = function (el, key) {
    el = $(el);
    el.keydown(utils.onlyAllowFloat);
    el.keypress(function (e) {
      if (e.which == 13) {
        e.preventDefault();
        var amount = parseFloat(el.val().trim());
        var query = utils.getQueryWithoutPageNumber();
        if (!_.isNaN(amount)) {
          query[key] = amount;
        } else {
          delete query[key];
        }
        window.location.search = $.param(query);
        el.data('value', !_.isNaN(amount) ? amount.toString() : '');
      }
    });
    el.focus(function () {
      el.data('value', el.val());
    });
    el.blur(function () {
      el.val(el.data('value'));
    });
  };

  utils.initSortableTableHead = function(tableHeadEl) {
    tableHeadEl = $(tableHeadEl);
    var sortDefault = tableHeadEl.data('sort-default') || {sortBy: null, sortOrder: 1};
    var query = utils.getQuery();
    var sort = _.extend({}, sortDefault, _.pick(query, 'sortBy', 'sortOrder'));
    _.each(tableHeadEl.find('th'), function(columnEl) {
      columnEl = $(columnEl);
      var sortBy = columnEl.data('sort-by');
      if (sortBy) {
        var text = columnEl.text().trim();
        columnEl.html('');
        var containerEl = $('<div class="sortable-column">' + text + '</div>');
        if (sortBy === sort.sortBy) {
          containerEl.addClass('selected');
          $('<i class="fa fa-lg fa-caret-' + (sort.sortOrder != -1 ? 'up' : 'down') + '"></i>').appendTo(containerEl);
        }
        containerEl.click(function() {
          var query = utils.getQueryWithoutPageNumber();
          query.sortBy = sortBy;
          query.sortOrder = sortBy === sort.sortBy ? -sort.sortOrder : sortDefault.sortOrder;
          window.location.search = $.param(query);
        });
        containerEl.appendTo(columnEl);
      }
    });
  };

  utils.initPageSizeDropdown = function(el) {
    $(el).change(function (e) {
      var query = utils.getQueryWithoutPageNumber();
      query.pageSize = $(e.target).val();
      if (!query.pageSize) {
        delete query.pageSize;
      }
      window.location.search = $.param(query);
    });
  };

  utils.setButtonLoading = function(button, replace) {
    button = $(button);
    // show loading status and save text
    var html = button.html().trim();
    button.data('original-html', html);
    if (!replace) {
      // append loading icon to original html
      button.html(html + '... <i class="fa fa-spinner fa-spin"></i>');
    } else {
      // do a complete replacement
      button.html('<i class="fa fa-spinner fa-spin"></i>');
    }
    button.prop('disabled', true);
  };

  utils.resetButtonLoading = function(button) {
    button = $(button);
    button.html(button.data('original-html'));
    button.data('original-html', null);
    button.prop('disabled', false);
  };

  utils.onlyAllowNumber = function(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl+A, Command+A
      (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode <= 40)) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  };

  function getOnlyAllowFloat(precision) {
    return function (e) {
      var input = $(e.target);
      var value = input.val();
      var selectionStart = input.prop('selectionStart');
      var selectionEnd = input.prop('selectionEnd');
      var index = value.indexOf('.');
      if (e.keyCode == 190) { // user press dot
        if (index >= 0) { // already has dot
          if (index < selectionStart || index >= selectionEnd) { // dot not in selection
            e.preventDefault();
            return;
          }
        } else {
          if (value.length - selectionEnd > precision) { // insert a dot at invalid position
            e.preventDefault();
            return;
          }
        }
      } else if (e.keyCode >= 48 && e.keyCode <= 57) { // user press number
        if (index >= 0 && index + precision < value.length) { // already has two digits after dot
          if (selectionStart === selectionEnd && selectionEnd > index) { // no selection and use try to edit fraction part
            e.preventDefault();
            return;
          }
        }
      }
      utils.onlyAllowNumber(e);
    };
  }

  utils.onlyAllowLongFloat = getOnlyAllowFloat(4);

  utils.onlyAllowFloat = getOnlyAllowFloat(2);

  utils.onlyAllowInt = function(e) {
    if (e.keyCode == 190) { // user press dot
      e.preventDefault();
      return;
    }
    utils.onlyAllowNumber(e);
  };

  utils.getDate = function(date) {
    var dateString = moment(date).format('YYYY-MM-DD');
    return moment(dateString + ' 00:00 +0000', 'YYYY-MM-DD HH:mm Z').toDate();
  };

  utils.back = function(urlProcessor) {
    var url = document.referrer;
    if (urlProcessor) {
      url = urlProcessor(url);
    }
    var version = 'v=' + Math.random();
    if (url) {
      var regex = /v=\d+/;
      var result = regex.exec(url);
      if (result) {
        url = url.substring(0, result.index) + version + url.substring(result.index + result[0].length);
      } else {
        if (url.indexOf('?') >= 0) {
          url += '&' + version;
        } else {
          url += '?' + version;
        }
      }
      window.location.href = url;
    } else {
      history.back();
    }
  };

  utils.reload = function(language) {
    var query = utils.getQuery();
    query.r = Math.random();
    if (language) {
      query.lang = language;
    }
    window.location.search = $.param(query);
  };

  /** Left Nav Expansion */

  var expansionButton = $('.left-nav-expansion');
  expansionButton.click(function () {
    if (expansionButton.data('opened')) {
      expansionButton.data('opened', false);
      $('.body-parts').removeClass('menu-opened');
      $('.mobile-menu').removeClass('menu-opened');
      $('.mobile-content').removeClass('menu-opened');
    } else {
      expansionButton.data('opened', true);
      $('.body-parts').addClass('menu-opened');
      $('.mobile-menu').addClass('menu-opened');
      $('.mobile-content').addClass('menu-opened');
    }
  });

  /** Navigation Group **/
  _.each($('.center-part .left-nav .nav-group'), function(groupEl) {
    groupEl = $(groupEl);
    var groupTitleEl = groupEl.find('.nav-group-title');
    var groupItemsEl = groupEl.find('.nav-group-items');
    var selectedIndicator = groupEl.find('.selected-indicator');
    var hasSelectedItem = _.findIndex(groupItemsEl.find('.nav-item'), function(navItemEl) {
      return $(navItemEl).hasClass('selected');
    }) >= 0;
    groupTitleEl.click(function() {
      if (groupItemsEl.hasClass('hidden')) {
        groupItemsEl.removeClass('hidden');
        groupTitleEl.find('i').removeClass('fa-caret-right').addClass('fa-caret-down');
        groupTitleEl.removeClass('selected');
        selectedIndicator.addClass('hidden');
      } else {
        groupItemsEl.addClass('hidden');
        groupTitleEl.find('i').removeClass('fa-caret-down').addClass('fa-caret-right');
        if (hasSelectedItem) {
          groupTitleEl.addClass('selected');
          selectedIndicator.removeClass('hidden');
        }
      }
    });
  });

})();

