'use strict';

var _ = require('underscore');
var uuid = require('node-uuid');
var path = require('path');
var fs = require('fs');
var ejs = require('ejs');

/*************************** INPUTS ************************************/

/**
 * Render inputs, options should have the following properties:
 * label - the display label of this field
 * name - the name of this field, it's used when aggregating values of different field to a single form data object
 * value - optional, the value of this field
 * placeholder - optional, can be used if the input supports placeholder
 * required - optional, if this input is required,
 * pattern - optional, for free form input, the regex to test if input is valid
 * patternErrorMessage, optional, the error message to show if pattern mismatches, if not set a general error message will be used
 * jsValidator - optional, the javascript validating function name, this function should either:
 *      1. returns the validated value, or throws the error message to show to user
 *      2. returns a promise that resolves to the validated value, or rejects with error message
 * hidden - optional, if the item should be hidden when first shown，
 * readonly - optional, if the item is readonly
 * other properties as per documentation of specific input type
 * */

/**
 * Renders static text
 * url - optional, the url
 */
function inputStatic(options) {
  if (_.isUndefined(options.value)) {
    console.error('static - options.value is required');
  }
  var content = ejs.render(inputStaticTemplate, {options: options});
  return renderInputField(options, 'static', content);
}

var inputStaticTemplate = _readTemplate('inputStaticTemplate');

/**
 * Renders hidden values
 */
function inputHidden(options) {
  return ejs.render(inputHiddenTemplate, {options: options});
}

var inputHiddenTemplate = _readTemplate('inputHiddenTemplate');

/**
 * Renders a plain text input
 */
function inputText(options) {
  var content = ejs.render(inputTextTemplate, {options: options});
  return renderInputField(options, 'text', content);
}

var inputTextTemplate = _readTemplate('inputTextTemplate');

/**
 * Renders a plain text input with action
 * actionLabels - optional, the label of action button
 * actionTimeout - optional, the timeout (in seconds) of action cooldown
 * jsAction - the javascript function for handling action button click, should return a promise which
 *        resolves when action successes or rejects with error message for action failure
 * hintHTML - optional, the hint html
 */
function inputTextWithAction(options) {
  if (!options.jsAction) {
    console.error('textWithAction - options.jsAction is required');
  }

  var content = ejs.render(inputTextWithActionTemplate, {options: options});
  return renderInputField(options, 'textWithAction', content);
}

var inputTextWithActionTemplate = _readTemplate('inputTextWithActionTemplate');

/**
 * Renders a password input
 */
function inputPassword(options) {
  var content = ejs.render(inputPasswordTemplate, {options: options});
  return renderInputField(options, 'password', content);
}

var inputPasswordTemplate = _readTemplate('inputPasswordTemplate');

/**
 * Renders a text area input, options have
 * rows - optional, specifies the rows of text area, defaults to 3
 */
function inputTextarea(options) {
  var content = ejs.render(inputTextareaTemplate, {options: options});
  return renderInputField(options, 'text', content);
}

var inputTextareaTemplate = _readTemplate('inputTextareaTemplate');

/**
 * Renders a rich text input, options have
 * summernoteOptions - optional, the options passed to summer note
 */
function inputRichText(options) {
  var content = ejs.render(inputRichTextTemplate, {options: options});
  return renderInputField(options, 'richText', content);
}

var inputRichTextTemplate = _readTemplate('inputRichTextTemplate');

/**
 * Renders select input, options have
 * selections - the selections for input like checkbox, radio, select, each selection can be string or object containing label and value.
 */
function inputSelect(options) {
  if (_.isUndefined(options.selections) || _.isEmpty(options.selections)) {
    console.error('select - options.selections is required');
  }

  var selections = _.map(options.selections, function (selection) {
    var label = _.isString(selection) ? selection : selection.label;
    var value = _.isString(selection) ? selection : selection.value;
    var selected = options.value === value;
    return {label: label, value: value, selected: selected};
  });
  var content = ejs.render(inputSelectTemplate, {options: _.extend({}, options, {selections: selections})});
  return renderInputField(options, 'select', content);
}

var inputSelectTemplate = _readTemplate('inputSelectTemplate');

/**
 * Renders checkbox input, options have
 * selections - the selections for input like checkbox, radio, select, each selection can be string or object containing label and value.
 */
function inputCheckbox(options) {
  if (_.isUndefined(options.selections) || _.isEmpty(options.selections)) {
    console.error('checkbox - options.selections is required');
  }

  var values = _.isArray(options.value) ? options.value : [options.value];
  var selections = _.map(options.selections, function (selection) {
    var label = _.isString(selection) ? selection : selection.label;
    var value = _.isString(selection) ? selection : selection.value;
    var selected = _.contains(values, value);
    return {label: label, value: value, selected: selected};
  });
  var content = ejs.render(inputCheckboxTemplate, {options: _.extend({}, options, {selections: selections})});
  return renderInputField(options, 'checkbox', content);
}

var inputCheckboxTemplate = _readTemplate('inputCheckboxTemplate');

/**
 * Renders radio input, options have
 * selections - the selections for input like checkbox, radio, select, each selection can be string or object containing label and value.
 */
function inputRadio(options) {
  if (_.isUndefined(options.selections) || _.isEmpty(options.selections)) {
    console.error('radio - options.selections is required');
  }

  var selections = _.map(options.selections, function (selection) {
    var label = _.isString(selection) ? selection : selection.label;
    var value = _.isString(selection) ? selection : selection.value;
    var selected = options.value === value;
    return {label: label, value: value, selected: selected};
  });
  var content = ejs.render(inputRadioTemplate, {options: _.extend({}, options, {selections: selections})});
  return renderInputField(options, 'radio', content);
}

var inputRadioTemplate = _readTemplate('inputRadioTemplate');

/**
 * Render the date time picker input.
 * Requirement: install this in your js/css first:
 * https://bootstrap-datepicker.readthedocs.io/en/stable/
 *
 * @params datePickerOptions the options to generate the datetime picker,
 * see: https://bootstrap-datepicker.readthedocs.io/en/stable/
 * @returns {String}
 */
function inputDate(options) {
  var content = ejs.render(inputDateTemplate, {options: options});
  return renderInputField(options, 'date', content);
}

var inputDateTemplate = _readTemplate('inputDateTemplate');

/**
 * Renders file input
 * multi - if it supports uploading multiple files
 * value - the value should be object is multi is false and be array if multi is true
 * uploadPath - required if jsUploader is not provided, the upload path
 * accept - optional, accepted file types
 * jsUploader - required if uploadPath is not provided, the javascript function to handle upload,
 *              the uploader should take a file as input arg, and return a promise which would resolve to value
 * jsUrlParser - optional, the javascript function to retrieve file url from value object, defaults to use the value as url
 * jsNameParser - optional, the javascript function to retrieve file name from value object, defaults to use the value as url
 */
function inputFile(options) {
  if (!options.uploadPath && !options.jsUploader) {
    console.error('file - either options.uploadPath or options.jsUploader is required');
  }
  if (options.uploadPath && options.jsUploader) {
    console.error('file - only one of options.uploadPath or options.jsUploader should be provided');
  }
  if (options.multi && !_.isUndefined(options.value) && !_.isArray(options.value)) {
    console.error('file - options.value should be array when options.multi is true');
  }
  if (!options.multi && !_.isUndefined(options.value) && _.isArray(options.value)) {
    console.error('file - options.value should not be array when options.multi is false');
  }

  var values = options.multi ? options.value : (options.value ? [options.value] : []);
  var addButtonHidden = (!options.multi && options.value) || options.readonly;
  var addButtonId = uuid.v4();
  var content = ejs.render(inputFileTemplate, {
    options: _.extend({}, options, {
      values: values,
      addButtonHidden: addButtonHidden,
      addButtonId: addButtonId
    })
  });
  return renderInputField(options, 'file', content);
}

var inputFileTemplate = _readTemplate('inputFileTemplate');

/**
 * Renders image input
 * multi - if it supports uploading multiple images
 * value - the value should be object is multi is false and be array if multi is true
 * uploadPath - required if jsUploader is not provided, the upload path
 * jsUploader - required if uploadPath is not provided, the javascript function to handle upload,
 *              the uploader should take a file as input arg, and return a promise which would resolve to value
 * jsUrlParser - optional, the javascript function to retrieve image url from value object, defaults to use the value as url
 */
function inputImage(options) {
  if (!options.uploadPath && !options.jsUploader) {
    console.error('image - either options.uploadPath or options.jsUploader is required');
  }
  if (options.uploadPath && options.jsUploader) {
    console.error('image - only one of options.uploadPath or options.jsUploader should be provided');
  }
  if (options.multi && !_.isUndefined(options.value) && !_.isArray(options.value)) {
    console.error('image - options.value should be array when options.multi is true');
  }
  if (!options.multi && !_.isUndefined(options.value) && _.isArray(options.value)) {
    console.error('image - options.value should not be array when options.multi is false');
  }

  var values = options.multi ? options.value : (options.value ? [options.value] : []);
  var addButtonHidden = (!options.multi && options.value) || options.readonly;
  var addButtonId = uuid.v4();
  var content = ejs.render(inputImageTemplate, {
    options: _.extend({}, options, {
      values: values,
      addButtonHidden: addButtonHidden,
      addButtonId: addButtonId
    })
  });
  return renderInputField(options, 'image', content);
}

var inputImageTemplate = _readTemplate('inputImageTemplate');

function renderInputField(options, type, content) {
  if (_.isUndefined(options.label)) {
    console.error(type + ' - options.label is required');
  }
  if (_.isUndefined(options.name)) {
    console.error(type + ' - options.name is required');
  }

  return ejs.render(inputFieldTemplate, {options: options, type: type, content: content});
}

var inputFieldTemplate = _readTemplate('inputFieldTemplate');

/*************************** REFERENCE ************************************/

/**
 * Render reference table, options should have the following properties:
 * label - the display label of this reference
 * name - the name of this reference, it's used when aggregating values of different field to a single form data object
 * columns - defines the columns (property name) to display data in reference table
 * values - optional, the values of this reference
 * min - optional, inclusive, the minimum number of values
 * max - optional, inclusive, the maximum number of values
 * jsEditor - the name of editor function, the function should accept a value argument which,
 *          edit existing value if value if provided or create new value if value is undefined
 *          The function should return a promise that resolves with the saved value or null if user cancels
 */

function inputReference(options) {
  if (_.isUndefined(options.label)) {
    console.error('reference - options.label is required');
  }
  if (_.isUndefined(options.name)) {
    console.error('reference - options.name is required');
  }
  if (_.isUndefined(options.columns) || _.isEmpty(options.columns)) {
    console.error('reference - options.columns is required');
  }
  if (_.isUndefined(options.jsEditor)) {
    console.error('reference - options.jsEditor is required');
  }

  //minDefined, maxDefined, hasValue
  return ejs.render(inputReferenceTemplate, {
    options: _.extend({}, options, {
      minDefined: !_.isUndefined(options.min),
      maxDefined: !_.isUndefined(options.max),
      hasValue: !_.isEmpty(options.values)
    })
  });
}

var inputReferenceTemplate = _readTemplate('inputReferenceTemplate');

function _readTemplate(templateName) {
  var file = path.join(__dirname, './templates/' + templateName + '.ejs');
  return fs.readFileSync(file, 'utf-8');
}

module.exports.inputStatic = inputStatic;
module.exports.inputHidden = inputHidden;
module.exports.inputText = inputText;
module.exports.inputTextWithAction = inputTextWithAction;
module.exports.inputPassword = inputPassword;
module.exports.inputTextarea = inputTextarea;
module.exports.inputRichText = inputRichText;
module.exports.inputSelect = inputSelect;
module.exports.inputCheckbox = inputCheckbox;
module.exports.inputRadio = inputRadio;
module.exports.inputImage = inputImage;
module.exports.inputFile = inputFile;
module.exports.inputReference = inputReference;
module.exports.inputDate = inputDate;
