'use strict';

var _ = require('underscore');
var path = require('path');
var fs = require('fs');
var ejs = require('ejs');

/**
 * Render pagination, options should have the following properties:
 * totalCount - the total number of items
 * totalPage - total page count
 * pageNumber - current page number
 * summary - optional, the summary text
 * */
function pagination(options) {
  if (_.isUndefined(options.totalCount) || _.isUndefined(options.totalPage) || _.isUndefined(options.pageNumber)) {
    console.error('pagination - options.totalCount, options.totalPage and options.pageNumber are all required');
  }
  return ejs.render(paginationTemplate, {options: options});
}

var paginationTemplate = _readTemplate('paginationTemplate');

/**
 * Render auto-complete filter, options should have the following properties:
 * label - optional, the label
 * lazyInit - optional, default to false, if true, it will need to be initialized by code manually
 * placeholder - optional, the placeholder
 * formattedText - optional, the formattedText for current value
 * value - optional, the value
 * freeForm - optional, default to false, whether user can input any text
 * jsFilter - the javascript function to return filtered value array based on user input,
 *            the function should take a string parameter and return a promise that will be resolved to value array
 * jsChangeHandler - optional, the javascript function to handle value change
 * jsTextFormatter - optional, the javascript function to format value to text displayed in text input
 * jsItemTextFormatter - optional, the javascript function to format value to text displayed as popup item
 * jsItemFormatter - optional, the javascript function to format value to item displayed in popup list
 */
function autoComplete(options) {
  if (_.isUndefined(options.jsFilter)) {
    console.error('auto complete filter - options.jsFilter are all required');
  }
  return ejs.render(autoCompleteTemplate, {options: options});
}

var autoCompleteTemplate = _readTemplate('autoCompleteTemplate');

function _readTemplate(templateName) {
  var file = path.join(__dirname, './templates/' + templateName + '.ejs');
  return fs.readFileSync(file, 'utf-8');
}

module.exports.pagination = pagination;
module.exports.autoComplete = autoComplete;
