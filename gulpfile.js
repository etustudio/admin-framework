'use strict';

var gulp = require('gulp');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var csswring = require('csswring');

gulp.task('css', function () {
  var processors = [
    autoprefixer({browsers: ['last 2 versions', '> 5%', 'Explorer >= 10', 'Android >= 2.3']}),
    csswring()
  ];
  return gulp.src('./src/css-src/**/*.css')
    .pipe(postcss(processors))
    .pipe(gulp.dest('./src/css/'));
});

gulp.task('watch', function () {
  // css
  gulp.watch('./src/css-src/**/*.css', ['css']);
});

gulp.task('src', ['css']);
