** To run demo:
1. npm install
2. bower install
3. node ./demo/www.js

** To include code:
1. git submodule add [admin-framework-repository-url] framework
2. in express configuration, expose framework/src as public resources
3. in express setup, require framework/src/renderer/input.js and expose it to app.locals
4. in express setup, require framework/src/renderer/misc.js and expose it to app.locals
5. in web page, include framework/src/css/core.css and framework/src/js/utils.js

** Changes to support mobile layout ***
1. /src/css/core.css has been updated
2. added "mobile-content" class to "center-parts" and "location-bar", refer to /demo/views/demo.ejs
3. added "mobile-menu" class to "left-nav", also added the "left-nav-expansion", refer to /demo/views/partial/left-nav.ejs
4. added javascript to expand / collapse menu, refer to codes in /src/js/utils.js (starting from line 966)
5. for '.form-item .value', use 'col-xs-12 col-sm-9' and 'col-xs-12 col-sm-3' for mobile layout, refer to /src/renderer/template/inputFieldTemplate.ejs
6. for input reference, in '.widget .title', group title text and ".message" div in a single parent div,
    so that in mobile layout, message can be shown in new line, refer to /src/renderer/template/inputReferenceTemplate.ejs