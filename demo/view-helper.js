'use strict';

var _ = require('underscore');

var version = 0.809;

function setUp(app) {
  app.locals.scriptTag = scriptTag;
  app.locals.cssTag = cssTag;
  _.extend(app.locals, require('../src/renderer/input.js'));
  _.extend(app.locals, require('../src/renderer/misc.js'));
}

function scriptTag(rawUrl) {
  var url = rawUrl;
  if (url.indexOf('http://') < 0 && url.indexOf('https://') < 0 && url.charAt(0) !== '/') { // relative url
    url = '/framework/' + rawUrl;
  }
  return '<script type="text/javascript" src="' + url + '?v=' + version + '"></script>';
}

function cssTag(rawUrl) {
  var url = '/framework/' + rawUrl;
  return '<link href="' + url + '?v=' + version + '" rel="stylesheet" />';
}

module.exports = {
  setUp: setUp
};
