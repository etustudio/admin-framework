'use strict';

var express = require('express');

var router = express.Router();

router.get(['/', '/list'], function (req, res, next) {
    res.render('list-demo');
});

router.get('/form', function (req, res, next) {
  var readonly = !!req.query.readonly;
  res.render('form-demo', {readonly: readonly});
});

router.get('/child1', function (req, res, next) {
  res.render('child1');
});

router.get('/child2', function (req, res, next) {
  res.render('child2');
});

module.exports = router;
