'use strict';

var app = require('./app');
var server = require('http').Server(app);

server.listen(3131, function () {
  console.log('Express server listening on port ' + server.address().port);
});
